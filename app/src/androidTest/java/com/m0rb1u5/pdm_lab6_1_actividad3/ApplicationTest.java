package com.m0rb1u5.pdm_lab6_1_actividad3;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
   public ApplicationTest() {
      super(Application.class);
   }
}